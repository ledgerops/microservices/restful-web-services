package com.in28minutes.rest.webservices.restfulwebservices.user;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.hateoas.EntityModel;
////import org.springframework.hateoas.Resource;
////import org.springframework.hateoas.mvc.ControllerLinkBuilder;
//import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
//
//import org.springframework.hateoas.Resource;
//
//import org.springframework.hateoas.mvc.ControllerLinkBuilder;
//import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
public class UserResource {

    @Autowired
    private UserDaoService service;

    // GET /users
    //retrieveAllUsers
    @GetMapping("/users")
    public List<User> retrieveAllUsers(){
        return service.findAll();
    }

//    @GetMapping("/users/{id}")
//    public EntityModel<User> retrieveUser(@PathVariable  int id){
//        User user =  service.findOne(id);
//
////       // Resource<User> resource = new Resource<User>(user);
//        EntityModel<User> model = new EntityModel<>(user);
//        WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retrieveAllUsers());
//        model.add(linkTo.withRel("all-users"));
//
//
////        Resource<User> resource = new Resource<User>(user);
////
////        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retrieveAllUsers());
////
////        resource.add(linkTo.withRel("all-users"));
//
//        if(user==null)
//            throw new UserNotFoundException("id-"+ id);
//        return model;
//    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable  int id){
        User user =  service.deleteOne(id);

        if(user==null)
            throw new UserNotFoundException("id-"+ id);
    }

    // input - details of user
    // output - CREATED & return the created URI

    @PostMapping("/users")
    public ResponseEntity<Object> createUser( @Valid  @RequestBody User user){
        User savedUser = service.save(user);

        //CREATED
        // /user
      URI location =   ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(savedUser.getId()).toUri();

        return ResponseEntity.created(location).build();
    }
}
