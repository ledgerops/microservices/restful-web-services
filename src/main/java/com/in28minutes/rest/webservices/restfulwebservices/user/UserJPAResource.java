package com.in28minutes.rest.webservices.restfulwebservices.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class UserJPAResource {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    // GET /users
    //retrieveAllUsers
    @GetMapping("/jpa/users")
    public List<User> retrieveAllUsers(){

        return userRepository.findAll();
    }

//    @GetMapping("/users/{id}")
//    public EntityModel<User> retrieveUser(@PathVariable  int id){
//        User user =  service.findOne(id);
//
////       // Resource<User> resource = new Resource<User>(user);
//        EntityModel<User> model = new EntityModel<>(user);
//        WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retrieveAllUsers());
//        model.add(linkTo.withRel("all-users"));
//
//
////        Resource<User> resource = new Resource<User>(user);
////
////        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retrieveAllUsers());
////
////        resource.add(linkTo.withRel("all-users"));
//
//        if(user==null)
//            throw new UserNotFoundException("id-"+ id);
//        return model;
//    }

    @GetMapping("/jpa/users/{id}")
    public User retrieveUser(@PathVariable int id){

        Optional<User> user = userRepository.findById(id);

        if (!user.isPresent()){
            throw new UserNotFoundException(" USer not found id-"+id);
        }

        //System.out.println("Printing user object : "+user.toString());
        return user.get();
    }

    @DeleteMapping("/jpa/users/{id}")
    public void deleteUser(@PathVariable int id){
        System.out.println("Id is : "+id);
        userRepository.deleteById(id);
    }

    // input - details of user
    // output - CREATED & return the created URI

    @PostMapping("/jpa/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user){
        User savedUser = userRepository.save(user);

        //CREATED
        // /user
        URI location =   ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(savedUser.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/jpa/users/{id}/posts")
    public List<Post> retrieveAllUserPost(@PathVariable int id) {
        Optional<User> userOptional =  userRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException("id - " + id);
        }
        return userOptional.get().getPosts();
    }


    @PostMapping("/jpa/users/{id}/posts")
    public ResponseEntity<Object> createPost(@PathVariable int id, @RequestBody Post post){
        Optional<User> userOptional =  userRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException("id - " + id);
        }

        User user = userOptional.get();

        post.setUser(user);
        Post savedPost = postRepository.save(post);

        //CREATED
        // /user
        URI location =   ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(savedPost.getId()).toUri();

        return ResponseEntity.created(location).build();
    }
}
